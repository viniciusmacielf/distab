Instalação
==========

Não é necessária instalação, basta baixar
o executável do DisTab [clicando aqui](https://drive.google.com/file/d/1x9PY3DmdlCFx41H1mHr9Gf-iihvKHHjD/view?usp=sharing) (para Windows x86-64)
ou seguindo o tópico abaixo.

Utilizando em GNU/Linux, Mac ou Windows x86
===============================================

É necessário ter instalado:
  - interpretador "python versão 3+"
  - instalador pip
  - com o pip instale módulo python "pyperclip"
  - com o pip instale módulo python "pyautogui"
  - com o pip instale módulo python "pynput"

Baixe este repositório, entre no terminal (CMD)
vá até a pasta baixada e execute a linha
abaixo:

```bash
$ python ./distribuir.py
```

Usabilidade
===========

Para usar o DisTab, basta executá-lo, conforme
descrito nos tópicos anteriores e:
  - Copie as notas de alguma fonte (como uma planilha)
  - Posicione o cursor sobre o primeiro campo de texto a receber a nota
  - Sem sair da tela onde a nota está sendo copiada, pressione F8
  - Acompanhe as notas sendo inseridas
  - Você pode repetir o processo sem fechar a aplicação.

Soluções alternativas para funcionar em distros GNU/Linux
=========================================================

Em algumas distros pode ser que seja
necessário instalar os módulos xsel e xclip
No caso do Debian com XFCE as linhas abaixo
são suficientes:

```bash
$ sudo apt-get install xsel
$ sudo apt-get install xclip
```
