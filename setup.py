#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import cx_Freeze as cx_f
import json, os

# python setup.py build

BUILD_PATH = os.path.join("dist", "DisTab")

executables = [cx_f.Executable(
        script="distribuir.py",
        targetName="DisTab.exe"
)]

options = {
    'build_exe': {
        'build_exe' : BUILD_PATH,
    }
}

cx_f.setup(name="DisTab", 
        version="2.0",
        author="ViniciusFM",
        options=options,
        description="This software is for special paste function with tabs.",
        executables=executables)
