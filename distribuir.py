#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import time
import pyautogui
import pyperclip
import re
from pynput import keyboard

NOTAREGEX = "\d*[.,]?\d+"
WAIT = 0.1

def imprimir_dicas(imprime_separador):
    if imprime_separador:
        print("\n----------------------------------------------------\n")
    print("Copie as notas a serem distribuídas.")
    print("Pressione F8 para disparar o distribuidor tábico")
    print("ou ESC para sair...")

def ler_notas():
    clipboard = pyperclip.paste()
    return re.findall(NOTAREGEX, clipboard)

def escrever_nota(n):
    time.sleep(WAIT)
    print("Escrevendo: "+n)
    pyautogui.typewrite(n)
    time.sleep(WAIT)
    pyautogui.press('tab')

def disparar_escrita(notas):
    for nota in notas:
        escrever_nota(nota)

def disparar_evento(key):
    if key == keyboard.Key.esc:
        return False
    elif key == keyboard.Key.f8:
        notas = ler_notas()
        print("Notas lidas: ", len(notas))
        disparar_escrita(notas)
        imprimir_dicas(True)

if __name__ == '__main__':
    imprimir_dicas(False)
    with keyboard.Listener(on_press=disparar_evento) as listener:
        listener.join()
